using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace socialMedia.ws.webServicesAPI.Controllers
{
    [Produces("application/json")]
    [Route("[Controller]")]
    public class TestController : Controller
    {
        static List<person> items = new List<person>();
        
        [HttpGet]
        public List<person> Get()
        {
           
            return TestController.items;
        }

        [HttpPost]
        public List<person> Post(string name, int age)
        {
            TestController.items.Add(new person(name,age));
            return TestController.items;
        }

        [Route("clear")]
        [HttpDelete]
        public List<person> Delete()
        {
            TestController.items.Clear();
            return TestController.items;
        }
    }

    public class person
    {
        public string name;
        public int age;

        public person(string name, int age)
        {
            this.name = name;
            this.age = age;
        }
    }
}