﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data;
namespace socialMedia.ws.dataAccessLayer
{
    /// <summary>
    /// Defines a Object that provide the Methods to work with the database of the Social Media Project.
    /// </summary>
    public class dbProvider
    {
        
        OracleCommand dbCommand;
        OracleDataAdapter dbAdapter;
        public OracleConnection dbConnection;
        DataSet output = new DataSet();
        public dbProvider()
        {
            dbConnection = new OracleConnection( "User id=SCOTT;" +
                                                 "Password=tiger;" +
                                                 "Data source=SCORE;" +
                                                 "Connection Timeout=120");
        }

        /// <summary>
        /// Retrieve a Oracle.ManagedDataAccess.Types.OracleRefCursor and returns a Table from the information
        /// stored in that OracleRefCursor.
        /// </summary>
        /// <param name="cursorObject"></param>
        /// <returns></returns>
        public DataTable getOracleCursorAsDataTable(OracleRefCursor cursorObject)
        {
            output = new DataSet();
            OracleDataAdapter dbAdapter = new OracleDataAdapter();
            dbAdapter.Fill(output, cursorObject);
            return output.Tables[0];
        }
        /// <summary>
        /// Get the expecify Data (ex. Rows and Columns) from a expecific Table.
        /// </summary>
        /// <param name="tableName">The Label that defines the table</param>
        /// <returns></returns>
        public DataTable getTableData(string tableName)
        {
            output = new DataSet();
            dbConnection.Open();
            dbCommand = new OracleCommand("SELECT * FROM " + tableName, dbConnection);
            dbAdapter = new OracleDataAdapter(dbCommand);
            try
            {
                if (dbCommand.ExecuteNonQuery() == 0) {
                    return new DataTable();
                }
                
            }
            catch
            {
                return new DataTable();
            }

            dbAdapter.Fill(output);
            dbConnection.Close();
            return output.Tables[0];
        }

        public DataTable getTenPostsById(int index)
        {
            output = new DataSet();
            dbCommand = new OracleCommand("PKG_SOC_MEDIA_RAFAEL.getTenPostByStartId", dbConnection);
            dbConnection.Open();
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.Parameters.Add("returnVal", OracleDbType.RefCursor);
            dbCommand.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;
            dbCommand.Parameters.Add("p_from", OracleDbType.Int16);
            dbCommand.Parameters["p_from"].Value = (decimal)index;
            dbCommand.ExecuteNonQuery();
            OracleRefCursor returnValue = ((OracleRefCursor)dbCommand.Parameters["returnVal"].Value);
            /*
            dbAdapter.Fill(output, returnValue);
            dbConnection.Close();
            return output.Tables[0];
            */
            return getOracleCursorAsDataTable(returnValue);
        }
    }


}
