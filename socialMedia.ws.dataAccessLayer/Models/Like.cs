﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socialMedia.ws.dataAccessLayer.Models
{
    public class Like
    {
        public int id { get; set; }
        public int postId { get; set; }
        public int userId { get; set; }
        public DateTime creationDate {get; set;}
    }
}
