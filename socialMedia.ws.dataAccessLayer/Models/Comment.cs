﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socialMedia.ws.dataAccessLayer.Models
{
    public class Comment
    {
        public int commentId { get; set; }
        public int userId {get; set;}
        public int postId { get; set; }
        public string commentData { get; set; }
        public DateTime creactionDate { get; set; }
    }
}
