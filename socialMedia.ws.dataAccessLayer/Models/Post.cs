﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socialMedia.ws.dataAccessLayer.Models
{
    public class Post
    {
        public int postId { get; set; }
        public int userId { get; set; }
        public string postData { get; set; }
        public DateTime creactionDate { get; set; }

    }
}
