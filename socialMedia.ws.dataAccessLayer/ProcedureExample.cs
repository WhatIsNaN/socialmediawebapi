﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data;
namespace ConnectingToOracle
{
    class Program
    {
        static OracleConnection Conn = new OracleConnection();
        static OracleCommand OracleAction;
        static OracleDataAdapter DataResult = new OracleDataAdapter();
        static DataSet db = new DataSet();
        static void Main(string[] args)
        {
            Connect();
            OracleAction = new OracleCommand("PKG_SOC_MEDIA_RAFAEL.getUserInfo", Conn);
            OracleAction.CommandType = CommandType.StoredProcedure;

            OracleAction.Parameters.Add("returnVal", OracleDbType.RefCursor);
            OracleAction.Parameters["returnVal"].Direction = ParameterDirection.ReturnValue;

            OracleAction.Parameters.Add("v_userId", OracleDbType.Int16);
            OracleAction.Parameters["v_userId"].Value = 1;

            OracleAction.ExecuteNonQuery();

            OracleRefCursor returnValue = ((OracleRefCursor)OracleAction.Parameters["returnVal"].Value);

            DataResult.Fill(db, returnValue);
            Console.WriteLine(db.Tables[0].Rows[0].Field<object>(1));

            /*
            DataResult = new OracleDataAdapter(OracleAction);
            OracleAction.ExecuteNonQuery();
            DataResult.Fill(db);
            Console.WriteLine(db.Tables[0].Rows[0].Field<object>(0));
            */

            Console.ReadKey();

        }

        private static void Connect()
        {
            Conn = new OracleConnection("User id=SCOTT;Password=tiger;Data source=SCORE;Connection Timeout=120");
            Conn.Open();
            Console.WriteLine("Connected to Oracle Server : " + Conn.ServerVersion);
            Console.WriteLine("Instance Name: " + Conn.InstanceName);

        }
    }
}
