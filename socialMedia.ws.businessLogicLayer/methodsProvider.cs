﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using socialMedia.ws.businessLogicLayer.Collections;
using socialMedia.ws.dataAccessLayer;
using socialMedia.ws.dataAccessLayer.Models;
using System.Data;
namespace socialMedia.ws.businessLogicLayer
{
    public class methodsProvider
    {
        dbProvider db = new dbProvider();
        /// <summary>
        /// returns a [Object Collection] of Users before queue to the Database.
        /// </summary>
        /// <returns></returns>
        public UsersCollection getAllUsers()
        {
            return new UsersCollection();
        }
        /// <summary>
        /// returns a [Object Collection] of Posts before queue to the Database.
        /// </summary>
        /// <returns></returns>
        public PostsCollections getAllPosts()
        {
            return new PostsCollections();
        }
        /// <summary>
        /// returns a [Object Collection] of 10 Posts choosen using a Start Id before queue to the Database.
        /// </summary>
        /// <returns></returns>
        public List<Post> getTenPostsAsList(int rowNum)
        {
           return wsDataParser.PostsDataRowsToPostObjectsCollection(db.getTenPostsById(rowNum).Rows);
        }
        
       

    }
}
