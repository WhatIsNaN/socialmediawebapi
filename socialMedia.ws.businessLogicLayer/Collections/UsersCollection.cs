﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using socialMedia.ws.dataAccessLayer.Models;
using socialMedia.ws.dataAccessLayer;
namespace socialMedia.ws.businessLogicLayer.Collections
{
    [Serializable]
    public class UsersCollection : List<User>
    {
        dbProvider DataDumper = new dbProvider();
        public UsersCollection()
        {
            foreach (DataRow value in DataDumper.getTableData("USERS_RAFAEL").Rows)
            {
                User user = new User();
                user.userId = Convert.ToInt16(value.Field<decimal>(0));
                user.userName = value.Field<string>(1);
                user.firstName = value.Field<string>(2);
                user.lastName = value.Field<string>(3);
                user.email = value.Field<string>(4);
                user.password = value.Field<string>(5);
                user.birthDate = value.Field<DateTime>(6);
                user.registrationDate = value.Field<DateTime>(7);
                this.Add(user);
                
           
            }
        }
    }
}
