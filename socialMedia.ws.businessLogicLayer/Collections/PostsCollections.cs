﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using socialMedia.ws.dataAccessLayer.Models;
using socialMedia.ws.dataAccessLayer;

namespace socialMedia.ws.businessLogicLayer.Collections
{
    [Serializable]
    public class PostsCollections : List<Post>
    {
        dbProvider DataDumper = new dbProvider();
        public PostsCollections()
        {
            foreach (DataRow value in DataDumper.getTableData("POSTS_RAFAEL").Rows)
            {
                Post post = new Post();
                post.postId = Convert.ToInt16(value.Field<decimal>(0));
                post.userId = Convert.ToInt16(value.Field<decimal>(1));
                post.postData = value.Field<string>(2);
                post.creactionDate = value.Field<DateTime>(3);
                this.Add(post);
            }

        }
    }
}
