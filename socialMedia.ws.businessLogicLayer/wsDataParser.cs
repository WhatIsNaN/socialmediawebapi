﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using socialMedia.ws.dataAccessLayer;
using socialMedia.ws.dataAccessLayer.Models;
using socialMedia.ws.businessLogicLayer.Collections;
using System.Data;
namespace socialMedia.ws.businessLogicLayer
{
    class wsDataParser
    {
        public static List<Post> PostsDataRowsToPostObjectsCollection(DataRowCollection collection)
        {
            List<Post> objList = new List<Post>();
            foreach (DataRow value in collection)
            {
                Post post = new Post();
                post.postId = Convert.ToInt16(value.Field<decimal>(0));
                post.userId = Convert.ToInt16(value.Field<decimal>(1));
                post.postData = value.Field<string>(2);
                post.creactionDate = value.Field<DateTime>(3);
                objList.Add(post);
            }
            return objList;
        }

        public static List<User> UsersDataRowsToPostsObjectsCollection(DataRowCollection collection)
        {
            List<User> objList = new List<User>();
            foreach (DataRow value in collection)
            {
                User user = new User();
                user.userId = Convert.ToInt16(value.Field<decimal>(0));
                user.userName = value.Field<string>(1);
                user.firstName = value.Field<string>(2);
                user.lastName = value.Field<string>(3);
                user.email = value.Field<string>(4);
                user.password = value.Field<string>(5);
                user.birthDate = value.Field<DateTime>(6);
                user.registrationDate = value.Field<DateTime>(7);
                objList.Add(user);
            }
            return objList;
        }

    }
}
