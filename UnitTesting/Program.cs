﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using socialMedia.ws.dataAccessLayer;
using socialMedia.ws.dataAccessLayer.Models;
using socialMedia.ws.businessLogicLayer.Collections;
using System.Xml.Serialization;
using Oracle.ManagedDataAccess;
using Newtonsoft.Json;
using System.Data;
namespace UnitTesting
{

    class Program
    {
        static dbProvider testingDatabase = new dbProvider();
        static void Main(string[] args)
        {
            TestConnection();
            TestUsersSelectMethod();
            testUserCollectionObject();
            testPostscollectionObject();
            testMethodGetTenPostsById();
            Console.ReadKey();

        }

        static void TestConnection()
        {
            testingDatabase.dbConnection.Open();
            Console.WriteLine(testingDatabase.dbConnection.State);
            Console.WriteLine(testingDatabase.dbConnection.ServiceName);
            Console.WriteLine(testingDatabase.dbConnection.ServerVersion);
            Console.WriteLine(testingDatabase.dbConnection.DatabaseName);
            testingDatabase.dbConnection.Close();
        }

        static void TestUsersSelectMethod()
        {
            Console.WriteLine(testingDatabase.getTableData("USERS_RAFAEL").Rows.Count.ToString() + 
                                                           " Rows in the USERS_RAFAEL Table");
        }

        static void testUserCollectionObject()
        {
            Console.WriteLine("Users Object: ");
            UsersCollection usr = new UsersCollection();
            foreach (object a in usr)
            {
                Console.WriteLine(JsonConvert.SerializeObject(a));
            }
        }
        static void testPostscollectionObject()
        {
            Console.WriteLine("Posts Object: ");
            PostsCollections pts = new PostsCollections();

            foreach (object a in pts)
            {
                Console.WriteLine(JsonConvert.SerializeObject(a));
            }
        }

        static void testMethodGetTenPostsById()
        {
            foreach (DataRow a in testingDatabase.getTenPostsById(1).Rows)
            {
                Console.WriteLine(a.Field<decimal>(0));
            }
        }

    }
}
